#!/usr/bin/env python

"""
Created on Mon Apr 11 05:59:43 2016
Modified on 22/10/2016
@author: grants

Apply a canopy height random forest model n14 predictor variables to Landsat 5 and Landsat 7 imagery.

"""

import sys
import os
import argparse
import cPickle as pickle
import numpy
from rios import applier, fileinfo
import pdb
from sklearn.preprocessing import Imputer

def getCmdargs():
    """
    Get command line arguments
    """
    p = argparse.ArgumentParser()
    p.add_argument("--reffile", help="Input surface reflectance file")
    
    p.add_argument("--outfile", help="Name of output file (default is chm of reffile)")
    
    p.add_argument("--picklefile", default="rfrLCHM.cpickle_23102016_512_log2_n14",help="Input pickle file (default is %(default)s)")
    cmdargs = p.parse_args()
    
    if cmdargs.reffile is None:
	    p.print_help()
	    sys.exit()
	    
    return cmdargs


def mainRoutine():
    """
    Main routine
    
    """
    cmdargs = getCmdargs()
    infiles = applier.FilenameAssociations()
    outfiles = applier.FilenameAssociations()
    otherargs = applier.OtherInputs()
    otherargs.refnull =  32767
    
    infiles.imageExtent = cmdargs.reffile
    
        
    outfiles.extent = "./Temp_imageExtent_delete_this_file.tif"

    applier.apply(imageExtent, infiles, outfiles, otherargs)


def imageExtent(info, inputs, outputs, otherargs):
    
    imgExt = inputs.imageExtent[0]
    
    output = numpy.array([imgExt])
    
    outputs.extent = output
    
        
if __name__ == "__main__":
    mainRoutine()    


def main():
    """
    Main routine
    
    """
    cmdargs = getCmdargs()
    controls = applier.ApplierControls()
    infiles = applier.FilenameAssociations()
    outfiles = applier.FilenameAssociations()
    otherargs = applier.OtherInputs()
    
    infiles.sfcref = cmdargs.reffile
    
    imageExt = infiles.sfcref
       
    infiles.dem = "./Temp_imageExtent_delete_this_file.tif"
    
        
    controls.setReferenceImage(infiles.sfcref) 
    
    outfiles.hgt = cmdargs.outfile
    
    otherargs.rf = pickle.load(open(cmdargs.picklefile))
    # refInfo = fileinfo.ImageInfo(infiles.sfcref)
    
    
    otherargs.refnull =  32767 
    

    applier.apply(doModel, infiles, outfiles, otherargs,controls=controls)
    
    # clean up the template image layer from the directory the code is being executed from
    os.remove("./Temp_imageExtent_delete_this_file.tif")
    
    # needed if script is run on a windows system
    #os.remove("Temp_imageExtent_delete_this_file.tif.aux.xml")
    
    
def doModel(info, inputs, outputs, otherargs):

	"""
	Called from RIOS.
	Apply the random forest model for canopy height
	"""
	nonNullmask = (inputs.sfcref[0] != otherargs.refnull)
	# this bit produces the shape for the output image currently the dem 
	dem = inputs.dem[0][nonNullmask]
	imgShape = inputs.dem.shape
	
	psB2 = inputs.sfcref[1][nonNullmask]
	psB3 = inputs.sfcref[2][nonNullmask]
	psB4 = inputs.sfcref[3][nonNullmask]
	psB5 = inputs.sfcref[4][nonNullmask]
	psB6 = inputs.sfcref[5][nonNullmask]
	
	psB2fp = psB2.astype(numpy.float32)
	psB3fp = psB3.astype(numpy.float32)
	psB4fp = psB4.astype(numpy.float32)
	psB5fp = psB5.astype(numpy.float32)
	psB6fp = psB6.astype(numpy.float32)
	
	psB2f = (psB2fp /10000.0).astype(numpy.float32)
	psB3f = (psB3fp /10000.0).astype(numpy.float32)
	psB4f = (psB4fp /10000.0).astype(numpy.float32)
	psB5f = (psB5fp /10000.0).astype(numpy.float32)
	psB6f = (psB6fp /10000.0).astype(numpy.float32)
	
	
	
	ratio42 = (psB4f / psB2f).astype(numpy.float32)
	GSAVI = (((psB4f-psB2f)/(psB4f+psB2f+0.5))*(1.5)).astype(numpy.float32)
	NDII = ((psB4f-psB5f)/(psB4f+psB5f)).astype(numpy.float32)
	ratio53 = (psB5f / psB3f).astype(numpy.float32)
	ratio64 = (psB6f / psB4f).astype(numpy.float32)
	ratio52 = (psB5f / psB2f).astype(numpy.float32)
	ratio32 = (psB3f / psB2f).astype(numpy.float32)
	ratio62 = (psB6f / psB2f).astype(numpy.float32)
	ratio65 = (psB6f / psB5f).astype(numpy.float32)
	ratio63 = (psB6f / psB3f).astype(numpy.float32)
	
	#ratio43 = (psB4f / psB3f).astype(numpy.float32)
	
		
	allVars = numpy.vstack([ratio42,GSAVI,psB5, psB3,NDII,psB4,ratio53,psB2,ratio64,ratio52,ratio32,ratio62,ratio65,ratio63]).T
	# sets up the shape and dtype for the chm output
	outputs.hgt = numpy.zeros(imgShape, dtype=numpy.float32)
	
	# applies the rfr model to produce the chm layer
	
	if allVars.shape[0] > 0:
	
		# run check over the input data and replaces nan and infinity values
		allVars[numpy.isnan(allVars)] = 0.0
		allVars[numpy.isinf(allVars)] = 0.0
		
		hgt = otherargs.rf.predict(allVars)
		outputs.hgt[0][nonNullmask] = hgt
    

if __name__ == "__main__":
    main()
