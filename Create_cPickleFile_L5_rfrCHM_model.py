#!/usr/bin/env python

"""
Code to produce the cPickle file for the random forest regressor canopy height model for Landsat 5 and 7 sensor 
It returns a number of stats on the model fit and band importance score.

Author: Grant Staben
Date: 23/04/2016
modified: 22/10/2016
"""



# import the required modules
import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestRegressor as rfr
import cPickle


def rfrmodel():
    
    """
   function to read in csv file, fit the random forest regressor model,out put the importance score, model fit statistic and rfr model.
   """
    df = pd.read_csv('L5_TrainingCorr_VarsReduction.csv', header=0)
    df2 = df.dropna()

    # read in the selected predictor variables
    xdata1 = df2[['ratio42','GSAVI','psB5', 'psB3', 'NDII','psB4','ratio53','psB2', 'ratio64','ratio52','ratio32','ratio62','ratio65','ratio63']]
    
    X_1 = xdata1

     # read in the mean chm heights derived from lidar and convert it into a format read by sklearn random forest model
    ydata1 = df2[['MEAN_chm']]
    ydata2 = ydata1.values
    y_1 = ydata2.ravel()

    print y_1.shape
    print X_1.shape

     # set the paramaters for the random forest regressor model
    rfrModel_1 = rfr(n_estimators=512, oob_score=True,  max_features = 'log2', min_samples_split=1,n_jobs=-1) 

     # fit the model to the training data 
    rfrLCHM = rfrModel_1.fit(X_1, y_1)
     
    # calcualte the feature importance scores
    feature_importance = rfrModel_1.feature_importances_    
    fi = enumerate(rfrModel_1.feature_importances_)
    cols = xdata1.columns 
    fiResult2 = [(value,cols[i]) for (i,value) in fi]
    
    fiResult = np.array(fiResult2)
    score = fiResult[:,0]
    band = fiResult[:,1]

    # calculate stats for the fit model
    fmr3 = format(rfrLCHM.score(X_1, y_1), '.2f')
    print 'Fitted model r2 =' ,  format(rfrLCHM.score(X_1, y_1), '.2f')
    fmrMSE3 = format(np.mean((y_1 - rfrLCHM.predict(X_1))**2), '.2f')
    print 'Fitted model mse =', format(np.mean((y_1 - rfrLCHM.predict(X_1))**2), '.2f')
    print 'n =', len(y_1)
     
    fmr4 = float(fmr3)
    fmMSE4 = float(fmrMSE3)
    
     
    return (score,band, fmr4,fmMSE4,rfrLCHM)

def importance_score(score,band):
    """
   function to sort and convert importance score data into a pandas df
   """
    # concatenate lists and transpose 
    fit_score = np.vstack([score, band]).T
    
    # create an empty pandas dataframe to append the stats in each csv files.
    score = pd.DataFrame(fit_score, columns=['score','band'])
    # sort importance score in decending order base on score value
    score = score.sort_values(by = 'score',ascending=False)
    
    return (score)


def mainRoutine():

    """
    run the main routine outputs the model fit stats and band importance scores
    and a cpickle file to apply to imagery
    """
    score,band, fmr4, fmMSE4,rfrLCHM = rfrmodel()
    
    df = importance_score(score,band)
    
    df.to_csv('L5ImportanceScore_model_512_log2_n14.csv')
    
    stats = np.vstack([fmr4,fmMSE4]).T
    
    df2 = pd.DataFrame(stats, columns=['r2','mse'])
    
    df2.to_csv('L5_Model_stats_model_512_log2_n14.csv')
    
    
    # save out model to a cPickle file 
    with open('rfrLCHM.cpickle_23102016_512_log2_n14', 'wb') as f:
        cPickle.dump(rfrLCHM, f)


if __name__ == "__main__":
    mainRoutine()